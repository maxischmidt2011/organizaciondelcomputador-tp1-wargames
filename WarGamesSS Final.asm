org 100h
jmp inicio

saltar_linea        db 10,13,'$'  ;salto de linea

;DIMENSIONES DEL TABLERO
ancho_tablero       db 79 
alto_tablero        db 21
borde_derecho       db ?
borde_izquierdo     db ?

;POSICION MENSAJE
msj_posX            EQU 1
msj_posY            EQU 22
msj_turno_posX      EQU 1
msj_turno_posY      EQU 23

;POSICION INICIAL
cursor_X_USA        EQU 55
cursor_Y_USA        EQU 10
cursor_X_URSS       EQU 19
cursor_Y_URSS       EQU 10
bases_URSS          db 43  ;cantidad de bases W
bases_USA           db 43  ;cantidad de bases W
                                                                      
;POSICION DE DISPARO
disparo_X           db ?
disparo_Y           db ?
turno               db ?

;MENSAJES                                             
msj_vacio           db "                         $",10,13,      ;es mas corto para generar la cascada
msj_gano_USA        db "*********************************GANO USA*********************************","$",10,13,
msj_gano_URSS       db "*********************************GANO URSS********************************","$",10,13,
msj_turno_USA       db "                               < TURNO USA >                              $"
msj_turno_URSS      db "                              < TURNO URSS >                              $"
msj_col_base_USA    db ">>> USA, coloca tu base secreta en el mapa.  $"
msj_col_base_URSS   db ">>> URSS, coloca tu base secreta en el mapa. $"

                    
tablero             db "*******************************************************************************",10,13,                     
                    db "***.........................WAR GAMES - 1983.................................**",10,13,
                    db "*******************************************************************************",10,13,                    
                    db "01*.......-.....:++:::+=-..-+++-:......**.....:--::=WWW++++-++-..............**",10,13,
                    db "02*...:WWWWWWWW=WWW=:::+:..::...--.....**+W==WWWWWWWWWWWWWWWWWWWWWWWW+-......**",10,13,
                    db "03*.-.....:WWWWWWWW=-=WW+..........--..**:+=++++++++WWWWWWWWWWWW:..:=........**",10,13,
                    db "04*.......+WWWWWWW+WWW=-:-..........-++**::::=+++=++++++WWW++:+++=-..........**",10,13,
                    db "05*......+WWWWWWWWW=...............::..**--+++::-++:::++++++++:--..-.........**",10,13,
                    db "06*.......:++WWW+=................-++++**:::-:+::++++++:++++++++.............**",10,13,
                    db "08*........-+:...-...............:+++++**+:++-++::-.-++++::+:::-.............**",10,13,
                    db "09*..........--:-................::++:+**++++:-+:.....::...-+:...-...........**",10,13,
                    db "10*..............-+++:-...........:+::+**++++++:-......-....-...---..........**",10,13,
                    db "11*..............:::++++:-.............**+++:+:.............:--+--.-.........**",10,13,
                    db "12*..............-+++++++++:...........**:+::+................--.....---.....**",10,13,
                    db "13*................:++++++:............**::+::.:-................-++:-:......**",10,13,
                    db "14*.................++::+-.............**:++:..:...............++++++++-.....**",10,13,
                    db "15*.................:++:-..............**::-..................-+:--:++:......**",10,13,
                    db "16*.................:+-................**...........................-.......-**",10,13,
                    db "17*.................:..................**...................................-**",10,13,
                    db "18*................USA.................**...............URSS.................**",10,13, 
                    db "*******************************************************************************",'$'                         

tablero2            db "*******************************************************************************"                     
                    db "***.........................WAR GAMES - 1983.................................**"
                    db "*******************************************************************************"                    
                    db "01*.......-.....:++:::+=-..-+++-:......**.....:--::=WWW++++-++-..............**"
                    db "02*...:WWWWWWWW=WWW=:::+:..::...--.....**+W==WWWWWWWWWWWWWWWWWWWWWWWW+-......**"
                    db "03*.-.....:WWWWWWWW=-=WW+..........--..**:+=++++++++WWWWWWWWWWWW:..:=........**"
                    db "04*.......+WWWWWWW+WWW=-:-..........-++**::::=+++=++++++WWW++:+++=-..........**"
                    db "05*......+WWWWWWWWW=...............::..**--+++::-++:::++++++++:--..-.........**"
                    db "06*.......:++WWW+=................-++++**:::-:+::++++++:++++++++.............**"
                    db "08*........-+:...-...............:+++++**+:++-++::-.-++++::+:::-.............**"
                    db "09*..........--:-................::++:+**++++:-+:.....::...-+:...-...........**"
                    db "10*..............-+++:-...........:+::+**++++++:-......-....-...---..........**"
                    db "11*..............:::++++:-.............**+++:+:.............:--+--.-.........**"
                    db "12*..............-+++++++++:...........**:+::+................--.....---.....**"
                    db "13*................:++++++:............**::+::.:-................-++:-:......**"
                    db "14*.................++::+-.............**:++:..:...............++++++++-.....**"
                    db "15*.................:++:-..............**::-..................-+:--:++:......**"
                    db "16*.................:+-................**...........................-.......-**"
                    db "17*.................:..................**...................................-**"
                    db "18*................USA.................**...............URSS.................**" 
                    db "*******************************************************************************",'$'    


lincoln             db "",10,13
                    db "**********************************GANO USA**************************************",10,13,                       
                    db "              H|H|H|H|H           H__________________________________     ",10,13,
                    db "              H|H|H|H|H           H|* * * * * *|---------------------|    ",10,13,
                    db "              H|H|H|H|H           H| * * * * * |---------------------|    ",10,13,
                    db "              H|H|H|H|H           H|* * * * * *|---------------------|    ",10,13,
                    db "              H|H|H|H|H           H| * * * * * |---------------------|    ",10,13,
                    db "              H|H|H|H|H           H|---------------------------------|    ",10,13,
                    db "           ===============        H|---------------------------------|    ",10,13,
                    db "             /| _   _ |\          H|---------------------------------|    ",10,13,
                    db "             (| O   O |)          H|---------------------------------|    ",10,13,
                    db "             /|   U   |\          H-----------------------------------    ",10,13,
                    db "              |  \=/  |           H                                       ",10,13,
                    db "               \_..._/            H                                       ",10,13,
                    db "               _|\I/|_            H                                       ",10,13,
                    db "       _______/\| H |/\_______    H                                       ",10,13,
                    db "      /       \ \   / /       \   H                                       ",10,13,
                    db "     |         \ | | /         |  H                                       ",10,13,
                    db "     |          ||o||          |  H                                       ",10,13,
                    db "     |    |     ||o||     |    |  H                                       ",10,13,
                    db "     |    |     ||o||     |    |  H                                       ",10,13,
                    db "                                                                          ",10,13,
                    db "--------------------------------------------------------------------------------",'$'


martillo_hoz        db "",10,13
                    db "################################################################################",10,13,
                    db "##################||######################################||####################",10,13,
                    db "#################|..(####################################|..(###################",10,13,
                    db "#############*...,##,...#############################*...,##,...################",10,13,
                    db "################""""'*##################################""""'*##################",10,13,
                    db "###############|.,##,.|################################|.,##,.|#################",10,13,
                    db "##################################             #################################",10,13,
                    db "#####################.'^'########## GANO URSS ###############.'^'###############",10,13,
                    db "#######################*. ''######             ################*. ''############",10,13,
                    db "#############*'  ..######*. '*#######################*'  ..######*. '*##########",10,13,
                    db "##########**'   .##########|  '*##################**'   .##########|  '*########",10,13,
                    db "#########).  ._. ''#########|  '#################).  ._. ''#########|  '########",10,13,
                    db "###########*#####..'*#######|   |##################*#####..'*#######|   |#######",10,13,
                    db "###################.. ''####|   |##########################.. ''####|   |#######",10,13,
                    db "#####################,. '*#|   .#############################,. '*#|   .########",10,13,
                    db "#########)' ..'^^'#####).     .(#################)' ..'^^'#####).     .(########",10,13,
                    db "#######^'..###*..            '*################^'..###*..            '*#########",10,13,
                    db "#####)' .(#########vv____v**..  (############)' .(#########vv____v**..  (#######",10,13,
                    db "################################################################################","$"



inicio:     
  call imprimir_tablero    
                                                        
  call turno_aleatorio
  
  call base_secreta    
  
  call turno_aleatorio   
  
  call mensaje_turno
  
  call ingreso_tecla
 
    
  proc ingreso_tecla  
    ;compara con las teclas ingresadas por el usuario
    tecla:        
      mov ah,7
      int 21h    ;lee tecla ingresada  
        
      cmp al,0Dh ;ENTER para tirar bomba
      je tirar_bomba
      cmp al, "d"    
      je derecha
      cmp al, "a"    
      je izquierda
      cmp al, "s"    
      je abajo
      cmp al, "w"    
      je arriba
        
      jmp tecla  ;si no se apreto ninguna tecla correcta hace loop

    tirar_bomba:
      call explosion
      call cambiar_turno
      jmp tecla
     ;ret
        
proc explosion
    ;dl, 177 caracter explosion central
    ;dl, 176 caracter explosion  no central 
    
    mov bh, 0       ;numero de pagina
    mov ah, 3
    int 10h         ;obtengo la posicion el cursor.
        	        ; en dl=posicX 
        	        ;    dh=posicY 
    ;guardo posiciones del centro del disparo
    mov disparo_X, dl
    mov disparo_y, dh
    
    call disparo177 ;disparo centro-centro
    
    mov dl, disparo_X
    mov dh, disparo_Y
    dec dl
    call posicionar_cursor
    call disparo176 ;disparo centro-izq
    
    mov dl, disparo_X
    mov dh, disparo_Y
    inc dl
    call posicionar_cursor
    call disparo176 ;disparo centro-derecha
    
    mov dl, disparo_X
    mov dh, disparo_Y
    dec dh
    call posicionar_cursor
    call disparo176 ;disparo arriba-centro
    
    mov dl, disparo_X
    mov dh, disparo_Y
    inc dh
    call posicionar_cursor
    call disparo176 ;disparo abajo-centro
    
    mov dl, disparo_X
    mov dh, disparo_Y
    dec dh
    dec dl
    call posicionar_cursor
    call disparo176 ;disparo izquierda-arriba
    
    mov dl, disparo_X
    mov dh, disparo_Y
    inc dh
    inc dl
    call posicionar_cursor
    call disparo176 ;disparo derecha-abajo
    
    mov dl, disparo_X
    mov dh, disparo_Y
    inc dh
    dec dl
    call posicionar_cursor
    call disparo176 ;disparo abajo-izquierda
    
    mov dl, disparo_X
    mov dh, disparo_Y
    dec dh
    inc dl
    call posicionar_cursor
    call disparo176 ;disparo arriba-derecha

 ret    
    proc disparo177
       mov al, ancho_tablero       ;me posiciono en el comienzo del tablero
       mul dh    ; en dl=posicX 
        	     ;    dh=posicY
       mov si, ax                  ;muevo el resultado de la mult a SI
       xor dh, dh                  ;inicializo DH
       add si, dx                  ;sumo desplaz en X 
       mov bl, tablero2[si]        ;paso el char a bl
      
       cmp bl, 104                 ;h 
       je ganaste
       
       call restar_base

       mov dl, 177      
       mov ah,02h
       int 21h                     ;imprimo caracter 177dec ascii
     ret         
    endp
    
    proc disparo176 
       mov al, ancho_tablero       ;me posiciono en el comienzo del tablero
       mul dh    ; en dl=posicX 
        	     ;    dh=posicY
       mov si, ax                  ;muevo el resultado de la mult a SI
       xor dh, dh                  ;inicializo DH
       add si, dx                  ;sumo desplaz en X 
       mov bl, tablero2[si]        ;paso el char a bl
       
       call restar_base
       
       cmp bl, 176                 ;zona debilitada 176dec
       je disparo177
       cmp bl, 177                 ;zona destruida 177dec
       je terminar          
       cmp bl, "*"                 ;borde "*"
       je no_disparar 
       cmp bl, "H"                 ;base secreta debilitada "H"
       je ganaste      
       cmp bl, "h"                 ;base secreta en buenas condiciones "h"
       jne no_es_la_base
       je es_la_base                     
   
       
       cmp bl, "W"                 ;base "W"
       je restar_base
       
       terminar:       
       mov dl, 176      
       mov ah,02h
       int 21h
     ret 
    endp
    
    no_es_la_base:
      mov tablero2[si], 176
      jmp terminar
    es_la_base:
      mov tablero2[si], "H"
      jmp terminar
    no_disparar:
      ret
      
    proc restar_base
      cmp bl, "W"           
      jne fin
      ;mov tablero2[si], 176  
      cmp turno, 2  ;turno usa
      je restar_base_USA
      jne restar_base_URSS
      fin:
     ret
    endp
    restar_base_USA:
      mov bh, bases_USA       
      dec bh
      mov bases_USA, bh
      jmp fin
    restar_base_URSS:
      mov bh, bases_URSS       
      dec bh
      mov bases_URSS, bh
      jmp fin
    
    proc posicionar_cursor 
      mov bh, 0
      mov ah, 2
      int 10h
     ret
    endp
    
endp explosion    
  
  ganaste: 
    mov dh,msj_posY
    mov dl,msj_posX
    
    mov bh,0                    ;uso la pagina 0
    mov ah,2
    int 10h                     ;set cursor
    cmp turno,2
    mov cx, 22 
    jne gano_usa
    jmp gano_urss
    
    gano_usa:
     mov dx, offset msj_gano_USA
     push cx
     
     mov ah,09
     int 21h   
     
     mov dx, offset msj_vacio
     mov ah,09
     int 21h
      
     pop cx
     loop gano_usa
     
     mov dx, offset lincoln
     int 21h
     
     mov ax, 4ch
     int 21h
     
    gano_urss:
     push cx
     mov dx, offset msj_gano_URSS
  
     mov ah,09
     int 21h   
     
     mov dx, offset msj_vacio
     mov ah,09
     int 21h 
     pop cx
     loop gano_urss
     
     mov dx, offset martillo_hoz
     int 21h
     
     mov ax, 4ch
     int 21h
  ret
    
  ;;;;;;;;;;;;;;;;;;;;;;   
  ;impresion de tablero;
  ;;;;;;;;;;;;;;;;;;;;;; 
  proc imprimir_tablero 
    mov al,03h  ;pantalla modo text mode. 80x25. 16 colors. 8 pages
    mov ah,0  
    int 10h 
        
    mov dx, offset tablero
    mov ah,09h
    int 21h 

   ret
  endp       
    
  ;;;;;;;;;;;;;;;;; 
  ;turno aleatorio;
  ;;;;;;;;;;;;;;;;;
  proc turno_aleatorio                 
    mov ah, 2ch ; Esta interrupcion me entrega la hora fragmentada en los registros, 
    int 21h     ; CH(h) CL(m) DH(s),DL (centesimo seg)int 21h      
                                                  
    xor ah,ah
    mov al,dl   ;muevo los centesimos a AL
   
   ret                
  endp
  
  ;;;;;;;;;;;;;;;;; 
  ;cambio de turno;
  ;;;;;;;;;;;;;;;;;
  proc cambiar_turno  
    cmp bases_URSS, 0       ;evaluo si gano URSS por destruccion
    je ganaste
    cmp bases_USA, 0        ;evaluo si gano USA por destruccion
    je ganaste
    cmp turno, 2
    je turno_USA
    jne turno_URSS
   ret
  endp
  
  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; 
  ;mensaje turno y colocar cursor en posicion inicial turno;
  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
  proc mensaje_turno
    mov ah, 0
    cmp al,2        ;compara al con 2, par USA //impar URSS
    jp turno_USA    ;si es par
    jnp turno_URSS  ;si es impar
   
    turno_USA:
    mov turno, 1
    ;set cursor
    mov dh,msj_turno_posY
    mov dl,msj_turno_posX
    mov bh, 0   
    mov ah,2
    int 10h
    ;imprime el mensaje de TURNO USA!  
    mov dx, offset msj_turno_USA
    mov ah,09
    int 21h  
    ;Mover cursor a posic inicial turno
    mov dh, cursor_Y_USA
    mov dl, cursor_X_USA 
    mov borde_derecho, 76
    mov borde_izquierdo, 41
    mov bh, 0
    mov ah, 2
    int 10h
   ret
      
    turno_URSS:
    mov turno, 2
    ;set cursor
    mov dh,msj_turno_posY
    mov dl,msj_turno_posX
    mov bh, 0   
    mov ah,2
    int 10h 
    ;imprime el mensaje de TURNO URSS!
    mov dx, offset msj_turno_URSS
    mov ah,09
    int 21h
    ;Mover cursor a posic inicial turno
    mov dh, cursor_Y_URSS
    mov dl, cursor_X_URSS
    mov borde_derecho, 38
    mov borde_izquierdo, 3
    mov bh, 0
    mov ah, 2
    int 10h
   ret
  endp  
    
  ;;;;;;;;;;;;;;;;;;;;;;;
  ;movimiento del cursor;
  ;;;;;;;;;;;;;;;;;;;;;;;
  
  ;MOVER A LA IZQUIERDA CON "A"
  izquierda: 
    call mover_izq
	jmp tecla   	
  ret	    
  proc mover_izq
    push cx    ;guardo cx porque tengo interrupciones	
	mov bh, 0  ;pagina 0
	mov ah, 3  ;obtengo la posicion el cursor
	int 10h    ; en dl=posicX 
			   ;    dh=posicY	    
	cmp dl,borde_izquierdo   ;verifica si esta en el borde izquierdo 
	je borde_izq    ;si esta en el borde no se mueve
	dec dl     ;"muevo" el cursor una posicion a la izq
	mov ah,2
	int 10h    ; Posiciono el cursor 
	pop cx
  ret
    borde_izq:
	  pop cx	
    ret      
  endp mover_izq
  
  ;MOVER A LA DERECHA CON "D"
  derecha: 
    call mover_der
	jmp tecla   
  ret     
  proc mover_der
    push cx    ;guardo cx porque tengo interrupciones	
	mov bh, 0  ;pagina 0
	mov ah, 3  ;obtengo la posicion el cursor
	int 10h    ; en dl=posicX 
			   ;    dh=posicY	    

	cmp dl,borde_derecho  ;verifica si esta en el borde derecho 
	je borde_der        ;si esta en el borde no se mueve	
	inc dl     ;"muevo" el cursor una posicion a la der
	mov ah,2
	int 10h    ; Posiciono el cursor 
	pop cx
  ret 
	borde_der:
	  pop cx	
	ret      
  endp mover_der
    
  ;MOVER HACIA ARRIBA CON "w"
  arriba: 
    call mover_arriba
    jmp tecla  
  ret      
  proc mover_arriba
    push cx    ;guardo cx porque tengo interrupciones	
	mov bh, 0  ;pagina 0
	mov ah, 3  ;obtengo la posicion el cursor
	int 10h    ; en dl=posicX 
			   ;    dh=posicY	    
	cmp dh,3   ;verifica si esta en el borde arriba 
	je borde_arriba     ;si esta en el borde no se mueve
	dec dh     ;"muevo" el cursor una posicion abajo
	mov ah,2
	int 10h    ; Posiciono el cursor 
	pop cx
  ret
	borde_arriba:	
	  pop cx
	ret    
  endp mover_arriba
         
  ;MOVER HACIA ABAJO CON "S"
  abajo: 
    call mover_abajo
    jmp tecla  
  ret 	   
  proc mover_abajo 
    push cx    ;guardo cx porque tengo interrupciones	
	mov bh, 0  ;pagina 0
	mov ah, 3  ;obtengo la posicion el cursor
	int 10h    ; en dl=posicX 
			   ;    dh=posicY	    
	cmp dh,19   ;verifica si esta en el borde abajo
	je borde_abajo	   ;si esta en el borde no se mueve
	inc dh     ;"muevo" el cursor una posicion arriba
	mov ah,2
	int 10h    ; Posiciono el cursor 
	pop cx
  ret
    borde_abajo:	
      pop cx
	ret    
  endp mover_abajo
  




  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
  ;;  A partir de aqui se utilizan fragmentos del codigo principal para;;
  ;; la colocacion de la base secreta                                  ;;
  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


  
  ;;;;;;;;;;;;;;
  ;base secreta;
  ;;;;;;;;;;;;;;
  proc base_secreta 
    mov cx, 2
    call cursor_msj_base
    ;compara con las teclas ingresadas por el usuario
    tecla2:
     push cx        
     mov ah,7
     int 21h    ;lee tecla ingresada
     pop cx   
     ;estaba el pop cx   
     cmp al,0Dh ;ENTER para tirar bomba
     je colocar_base
     cmp al, "d"    
     je derecha2
     cmp al, "a"    
     je izquierda2
     cmp al, "s"    
     je abajo2
     cmp al, "w"    
     je arriba2
  
     jmp tecla2  ;si no se apreto ninguna tecla correcta hace loop
    
    colocar_base:
     push cx
     mov bh, 0       ;numero de pagina
     mov ah, 3
     int 10h         ;obtengo la posicion el cursor.     
     pop cx       
            
     mov disparo_X, dl  ;x
     mov disparo_y, dh  ;y
     
     mov al, ancho_tablero
     mul dh
     mov si, ax
     xor dh, dh
     add si, dx
     mov tablero2[si], "h"
     
     call cambiar_turno2
      
     loop tecla2
    
   ret
  endp  
   
  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
  ;mensaje colocar base (se ejecuta un sola vez);
  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
  proc cursor_msj_base
    mov ah, 0
    cmp al,2  ;compara al con 2, par USA //impar URSS
    jp turno_USA2   ;si es par
    jnp turno_URSS2 ;si es impar
   
    turno_USA2:
    mov turno, 1
    ;set cursor
    mov dh,msj_turno_posY
    mov dl,msj_turno_posX
    mov bh, 0   
    mov ah,2
    int 10h
    ;imprime el mensaje de TURNO USA!  
    mov dx, offset msj_col_base_USA 
    mov ah,09
    int 21h  
    ;Mover cursor a posic inicial turno
    mov dh, cursor_Y_URSS
    mov dl, cursor_X_URSS 
    mov borde_derecho, 38
    mov borde_izquierdo, 3
    mov bh, 0
    mov ah, 2
    int 10h
   ret
      
    turno_URSS2:
    mov turno, 2
    ;set cursor
    mov dh,msj_turno_posY
    mov dl,msj_turno_posX
    mov bh, 0   
    mov ah,2
    int 10h 
    ;imprime el mensaje de TURNO URSS!
    mov dx, offset msj_col_base_URSS
    mov ah,09
    int 21h
    ;Mover cursor a posic inicial turno
    mov dh, cursor_Y_USA
    mov dl, cursor_X_USA
    mov borde_derecho, 76
    mov borde_izquierdo, 41
    mov bh, 0
    mov ah, 2
    int 10h
   ret
  endp  
   
  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; 
  ;cambio de turno para la base;
  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
  proc cambiar_turno2  
    cmp turno, 2
    je turno_USA2
    jne turno_URSS2
   ret
  endp   
   
  ;;;;;;;;;;;;;;;;;;;;;;;
  ;movimiento del cursor;
  ;;;;;;;;;;;;;;;;;;;;;;;  
  ;MOVER A LA IZQUIERDA CON "A"
  izquierda2: 
    call mover_izq
	  jmp tecla2   	
     ret
    endp	  
  ;MOVER A LA DERECHA CON "D"
  derecha2: 
    call mover_der
	  jmp tecla2   
     ret     
    endp  
  ;MOVER HACIA ARRIBA CON "w"
  arriba2: 
    call mover_arriba
      jmp tecla2  
     ret        
    endp 
  ;MOVER HACIA ABAJO CON "S"
  abajo2: 
    call mover_abajo
      jmp tecla2  
     ret 	   
    endp
